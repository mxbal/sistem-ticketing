<table>
    <thead>
        <tr>
            <th>No</th>
            <th>No Transaksi</th>
            <th>Tanggal</th>
            <th>Nama Customer</th>
            <th>Ticket Code</th>
            <th>Type</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Bayar</th>
            <th>Discount</th>
            <th>Kembali</th>
            <th>Metode</th>
            <th>Kasir</th>
        </tr>
    </thead>

    <tbody>
        @foreach($transactions as $transaction)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $transaction->no_trx }}</td>
            <td>{{ Carbon\Carbon::parse($transaction->created_at)->format('d/m/Y H:i:s') }}</td>
            <td>{{ $transaction->nama_customer }}</td>
            <td>{{ $transaction->ticket_code }}</td>
            <td>{{ $transaction->tipe }}</td>
            <td>{{ $transaction->amount }}</td>
            <td>{{ $transaction->harga_ticket }}</td>
            <td>{{ $transaction->cash }}</td>
            <td>{{ $transaction->discount }}</td>
            <td>{{ $transaction->kembalian }}</td>
            <td>{{ $transaction->metode }}</td>
            <td>{{ $transaction->created_by }}</td>
        </tr>
        @endforeach
    </tbody>

    <tfoot>
        <tr>
            <td>Total : </td>
            <td colspan="5"></td>
            <td>{{ $transactions->sum('amount') }}</td>
            <td>{{ $transactions->sum('harga_ticket') }}</td>
            <td>{{ $transactions->sum('cash') }}</td>
            <td>{{ $transactions->sum('discount') }}</td>
            <td>{{ $transactions->sum('kembalian') }}</td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <th colspan="7">All Total : </th>
            <th>{{ $transactions->sum('harga_ticket') - $transactions->sum('discount') }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="7">Total Discount : </th>
            <th>{{ $transactions->sum('discount') }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="7">Total Debet : </th>
            <th>{{ $transactions->where('metode', 'debit')->sum('harga_ticket') }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="7">Total Other : </th>
            <th>{{ $transactions->where('metode', 'other')->sum('harga_ticket') }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="7">Total Tunai : </th>
            <th>{{ $transactions->where('metode', 'tunai')->sum('harga_ticket') }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th colspan="7">Grand Total Penerimaan Tunai : </th>
            <th>{{ $transactions->where('metode', 'tunai')->sum('harga_ticket') - $transactions->sum('discount')  }}</th>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>