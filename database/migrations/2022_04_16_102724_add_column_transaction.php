<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('no_trx')->after('id');
            $table->integer('ticket_id')->after('harga_ticket');
            $table->integer('kembalian')->after('ticket_id');
            $table->integer('cash')->after('kembalian');
            $table->integer('discount')->default(0)->after('cash');
            $table->string('metode')->after('discount');
            $table->integer('gate')->nullable()->after('metode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction', function (Blueprint $table) {
            $table->dropColumn('ticket_id');
            $table->dropColumn('kembalian');
            $table->dropColumn('cash');
            $table->dropColumn('discount');
            $table->dropColumn('metode');
            $table->dropColumn('gate');
        });
    }
}
