<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $fillable = [
        'name', 'harga'
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
