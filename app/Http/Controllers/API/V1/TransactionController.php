<?php

namespace App\Http\Controllers\API\V1;

use App\Exports\ReportExport;
use Illuminate\Http\Request;
use App\Exports\TransactionExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\TransactionRequest;
use App\Http\Resources\TicketResource;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;

class TransactionController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['exportExcel', 'checkIndividualTicket', 'checkGroupTicket', 'checkGroupTicketDua', 'getNoTrx', 'report', 'export']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $this->authorize('isAdmin');

        $transactions = Transaction::latest()->paginate(10);

        return $this->sendResponse($transactions, 'Transactions list');
    }

    public function report()
    {
        try {
            $from = Carbon::parse(request('from'))->format('Y-m-d');
            $to = Carbon::parse(request('to'))->addDay(1)->format('Y-m-d');

            $transactions = Transaction::with('ticket')->whereBetween('created_at', [$from, $to])->get();
            $data = Ticket::with('transactions')->get();

            $param = ['from' => $from, 'to' => $to];

            $tickets = TicketResource::collection($data);

            $total_amount = Transaction::whereBetween('created_at', [$from, $to])->sum('harga_ticket');
            $jumlah = Transaction::whereBetween('created_at', [$from, $to])->sum('amount');
            $tunai = Transaction::whereBetween('created_at', [$from, $to])->where('metode', 'tunai')->sum('harga_ticket');
            $debit = Transaction::whereBetween('created_at', [$from, $to])->where('metode', 'debit')->sum('harga_ticket');
            $other = Transaction::whereBetween('created_at', [$from, $to])->where('metode', 'other')->sum('harga_ticket');
            $discount = Transaction::whereBetween('created_at', [$from, $to])->sum('discount');
            $all = $total_amount - $discount;
            $allTunai = $tunai - $discount;

            return response()->json([
                'message' => 'Success',
                'tickets' => $tickets,
                'tunai' => $tunai,
                'debit' => $debit,
                'other' => $other,
                'discount' => $discount,
                'total_amount' => $total_amount,
                'jumlah' => $jumlah,
                'all' => $all,
                'allTunai' => $allTunai,
                'transactions' => $transactions,
                'from' => $from,
                'to' => $to,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TransactionRequest  $request
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(TransactionRequest $request)
    {

        $timestamp = Carbon::now()->timestamp;
        $user = User::where('id', Auth::id())->select(['name'])->first();
        $tipe = "IDV";
        $code = substr($request['name'], 1, 1) . substr($request['name'], strlen($request['name']) - 1, 1);
        if ($request['type'] == "group") {
            $tipe = "GRP";
        }
        $ticketCode = strtoupper("TKT-" . $tipe . $timestamp . $code);
        $now = Carbon::now()->format('Y-m-d');
        $getTransaction = Transaction::where('created_at', 'like', "%$now%")->orderBy('no_trx', 'DESC')->first();
        if ($getTransaction) {
            $noTrx = $getTransaction->no_trx + 1;
        } else {
            $noTrx = 1;
        }

        $transaction = Transaction::create([
            'nama_customer' => $request['name'],
            'no_trx' => $noTrx,
            'ticket_code' => $ticketCode,
            'amount' => $request['amount'],
            'tipe' => $request['type'],
            'status' => 'open',
            'amount_scanned' => 0,
            'harga_ticket' => $request['harga_ticket'],
            'kembalian' => $request['kembalian'],
            'discount' => ($request['harga_ticket'] * $request['discount']) / 100,
            'metode' => $request['metode'],
            'cash' => $request['cash'],
            'ticket_id' => $request['ticket'],
            'created_by' => $user->name,
        ]);

        return $this->sendResponse($transaction, 'Transaction Created Successfully');
    }

    /**
     * Update the resource in storage
     *
     * @param  \App\Http\Requests\TransactionRequest  $request
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(TransactionRequest $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->update($request->all());

        return $this->sendResponse($transaction, 'Transaction Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $transaction = Transaction::findOrFail($id);
        // delete the user

        $transaction->delete();

        return $this->sendResponse([$transaction], 'Transaction has been Deleted');
    }

    public function getCode()
    {

        // $this->authorize('isAdmin');

        $transactions = Transaction::select(['id', 'name'])->get();

        return $this->sendResponse($transactions, 'Transactions list');
    }

    public function exportExcel(Request $request)
    {
        $dateStart = Carbon::parse($request->date_start)->format('Y-m-d');
        $dateEnd = Carbon::parse($request->date_end)->addDay(1)->format('Y-m-d');

        return Excel::download(new TransactionExport($dateStart, $dateEnd), 'Transaction.xlsx');
    }

    public function export(Request $request)
    {
        $from = Carbon::parse(request('from'))->format('Y-m-d');
        $to = Carbon::parse(request('to'))->addDay(1)->format('Y-m-d');


        return Excel::download(new ReportExport($from, $to), 'Report Transaction.xlsx');
    }

    public function checkIndividualTicket($ticket)
    {

        $transScanned = Transaction::where('ticket_code', $ticket)->where('tipe', 'individual')
            ->select(['amount', 'amount_scanned', 'status'])->first();

        if (!$transScanned) {
            return response()->json([
                "status" => "not found"
            ]);
        }

        if ($transScanned->status == "closed") {
            return response()->json([
                "status" => $transScanned->status,
                "count" => 0
            ]);
        }

        $counting = $transScanned->amount_scanned + 1;
        if ($transScanned->amount == $counting) {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "status" => "closed",
                    "amount_scanned" => $counting
                ]);
        } else {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "amount_scanned" => $counting
                ]);
        }

        return response()->json([
            "status" => $transScanned->status,
            "count" => $transScanned->amount - $counting
        ]);
    }

    public function checkGroupTicket($ticket)
    {

        $transScanned = Transaction::where('ticket_code', $ticket)->where('tipe', 'group')
            ->select(['amount', 'amount_scanned', 'status'])->first();

        Transaction::where('ticket_code', $ticket)
            ->update([
                "gate" => 1,
            ]);

        if (!$transScanned) {
            return response()->json([
                "status" => "not found"
            ]);
        }


        if ($transScanned->status == "closed") {
            return response()->json([
                "status" => $transScanned->status,
                "count" => 0
            ]);
        }

        $counting = $transScanned->amount_scanned + 1;

        if ($transScanned->amount == $counting) {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "status" => "closed",
                    "amount_scanned" => $counting
                ]);
        } else {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "amount_scanned" => $counting
                ]);
        }

        return response()->json([
            "status" => $transScanned->status,
            "count" => $transScanned->amount - $counting
        ]);
    }

    public function checkGroupTicketDua($ticket)
    {

        $transScanned = Transaction::where('ticket_code', $ticket)->where('tipe', 'group')
            ->select(['amount', 'amount_scanned', 'status'])->first();

        Transaction::where('ticket_code', $ticket)
            ->update([
                "gate" => 2,
            ]);

        if (!$transScanned) {
            return response()->json([
                "status" => "not found"
            ]);
        }


        if ($transScanned->status == "closed") {
            return response()->json([
                "status" => $transScanned->status,
                "count" => 0
            ]);
        }

        $counting = $transScanned->amount_scanned + 1;

        if ($transScanned->amount == $counting) {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "status" => "closed",
                    "amount_scanned" => $counting
                ]);
        } else {
            Transaction::where('ticket_code', $ticket)
                ->update([
                    "amount_scanned" => $counting
                ]);
        }

        return response()->json([
            "status" => $transScanned->status,
            "count" => $transScanned->amount - $counting
        ]);
    }

    public function getNoTrx()
    {
        $now = Carbon::now()->format('Y-m-d');
        $transaction = Transaction::where('created_at', 'like', "%$now%")->orderBy('no_trx', 'DESC')->first();
        if ($transaction) {
            $noTrx = $transaction->no_trx + 1;
        } else {
            $noTrx = 1;
        }

        return response()->json([
            "no_trx" => $noTrx,
        ]);
    }
}
