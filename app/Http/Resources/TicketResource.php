<?php

namespace App\Http\Resources;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    public function toArray($request)
    {
        $from = $request->get('from');
        $to = Carbon::parse($request->get('to'))->addDay(1)->format('Y-m-d');

        return [
            'id' => $this->id,
            'name' => $this->name,
            'harga' => $this->harga,
            'from' => $from,
            'to' => $to,
            'jumlah' => Transaction::whereBetween('created_at', [$from, $to])->where('ticket_id', $this->id)->sum('amount'),
            'total' => Transaction::whereBetween('created_at', [$from, $to])->where('ticket_id', $this->id)->sum('harga_ticket'),
            'totalDisc' => Transaction::whereBetween('created_at', [$from, $to])->where('ticket_id', $this->id)->sum('discount'),
        ];
    }
}
