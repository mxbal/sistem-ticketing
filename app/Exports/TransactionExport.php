<?php

namespace App\Exports;

use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TransactionExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $date_start, $date_end;

    function __construct($date_start, $date_end)
    {
        $this->date_start = $date_start;
        $this->date_end = $date_end;
    }

    public function view(): View
    {
        $data = Transaction::whereBetween('created_at', [$this->date_start, $this->date_end])
            ->orderBy('created_at')
            ->get();

        return view('export', [
            'transactions' => $data
        ]);
    }
}
